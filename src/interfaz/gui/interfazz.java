package interfaz.gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Image;

import javax.swing.border.EmptyBorder;

import javax.swing.*;
import javax.imageio.*;

import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.awt.event.ActionEvent;
import java.awt.Color;
import java.awt.SystemColor;
import javax.swing.border.LineBorder;

import procesamiento.op.ImageOperator;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;

public class interfazz extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	BufferedImage img1;
	BufferedImage img2;
	BufferedImage imgFINAL;
	ImageOperator img;
	Icon iconNew;
	private JTextField textNombreArch;
	private JTextField textField;
	private JTextField textField_1;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					interfazz frame = new interfazz();
					frame.setVisible(true);
					
					UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public interfazz() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 992, 450);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel label = new JLabel("__________________________________________");
		label.setBounds(331, 244, 308, 14);
		contentPane.add(label);
		
		JLabel lblImagen1 = new JLabel("Imagen 1");
		lblImagen1.setBackground(Color.white);
		lblImagen1.setBounds(21, 23, 300, 225);
		contentPane.add(lblImagen1);

		JLabel lblImagen2 = new JLabel("Imagen 2");
		lblImagen2.setBounds(339, 23, 300, 225);
		contentPane.add(lblImagen2);
		
		JLabel lblFinal = new JLabel("Final");
		lblFinal.setBounds(657, 23, 300, 225);
		contentPane.add(lblFinal);
		
		textNombreArch = new JTextField();
		textNombreArch.setBounds(710, 281, 134, 20);
		contentPane.add(textNombreArch);
		textNombreArch.setColumns(10);
		
		JSlider slider = new JSlider();
		slider.setMinorTickSpacing(1);
		slider.setValue(0);
		slider.setPaintLabels(true);
		slider.setPaintTicks(true);
		slider.setMaximum(10);
		slider.setBounds(216, 330, 227, 59);
		contentPane.add(slider);
		slider.setVisible(true);
		
		textField = new JTextField();
		textField.setBounds(573, 332, 86, 20);
		contentPane.add(textField);
		textField.setColumns(10);
		
		textField_1 = new JTextField();
		textField_1.setBounds(573, 301, 86, 20);
		contentPane.add(textField_1);
		textField_1.setColumns(10);
		
		JButton btnElegirImg1 = new JButton("Elegir im\u00E1gen");
		btnElegirImg1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				JFileChooser btnCargar = new JFileChooser();
				btnCargar.setDialogTitle("Seleccionar Im�gen");
				
				if(btnCargar.showOpenDialog(getParent())== JFileChooser.APPROVE_OPTION)
				{
					///File archivo = new File(btnCargar.getSelectedFile().toString());
					Image image;
					try {
						//Obteniendo la imagen de archivos
						image = ImageIO.read(btnCargar.getSelectedFile());
						
						//Imagen para procesar despues
						img1 = ImageIO.read(btnCargar.getSelectedFile());
						
						//Pasando a icono para poder a�adir al label
						Icon icon = new ImageIcon(image);
						
						lblImagen1.setIcon(icon);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
				}
			}
		});
		btnElegirImg1.setBounds(90, 275, 120, 33);
		contentPane.add(btnElegirImg1);
		
		JLabel label_1 = new JLabel("__________________________________________");
		label_1.setBounds(21, 244, 308, 14);
		contentPane.add(label_1);
		
		JLabel label_2 = new JLabel("__________________________________________");
		label_2.setBounds(649, 244, 308, 14);
		contentPane.add(label_2);
		
		JButton btnCargarImgen = new JButton("Elegir Im\u00E1gen 2");
		btnCargarImgen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				JFileChooser btnCargar = new JFileChooser();
				btnCargar.setDialogTitle("Seleccionar Im�gen");
		
				if(btnCargar.showOpenDialog(getParent())== JFileChooser.APPROVE_OPTION)
				{
					Image image;
					try {
						image = ImageIO.read(btnCargar.getSelectedFile());
						img2 = ImageIO.read(btnCargar.getSelectedFile());
						
						Icon icon = new ImageIcon(image);
						//int i = img2.getHeight();
						//lblFinal.setText(String.valueOf(i));
						lblImagen2.setIcon(icon);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
				}
			}
		});
		btnCargarImgen.setBounds(401, 275, 120, 33);
		contentPane.add(btnCargarImgen);
		
		JComboBox cboxOperador = new JComboBox();
		cboxOperador.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				String valor = arg0.getItem().toString();
				//System.out.print(valor);
				if(valor == "Todas" || valor == "#")
					slider.setVisible(true);
				else{
					slider.setVisible(false);
				}
			}
		});
		
		cboxOperador.setModel(new DefaultComboBoxModel(new String[] {"Todas", "+", "-", "*", "#"}));
		cboxOperador.setBounds(284, 281, 55, 20);
		contentPane.add(cboxOperador);
		
		JButton btnGenerar = new JButton("Generar");
		btnGenerar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			img = new ImageOperator(img1,img2);	
			int horizontal = Integer.parseInt(textField.getText());
			int vertical = Integer.parseInt(textField_1.getText());
			
			int number = slider.getValue();
			float alpha = number*0.1f;
			
				if(!textNombreArch.getText().isEmpty())
				{
					String nombre=textNombreArch.getText();
					int i = cboxOperador.getSelectedIndex();
					
					switch(i)
					{
						case 0:
						{
							imgFINAL = img.generarBloques(vertical, horizontal, alpha);
							
						}
							break;
						case 1:
						{
							imgFINAL = img.generarBloques_Op(vertical, horizontal, 0, 0);
						}
							break;
						case 2:
						{
							imgFINAL = img.generarBloques_Op(vertical, horizontal, 0, 1);
						}
							break;
						case 3:
						{
							imgFINAL = img.generarBloques_Op(vertical, horizontal, 0, 2);
						}
							break;
						case 4:
						{
							imgFINAL = img.generarBloques_Op(vertical, horizontal, alpha, 3);
						}
							break;

					}
					
					String rutaFile = "C:/Users/Lenovo/Desktop/";
					rutaFile+=nombre;
					rutaFile+=".jpg";
					
					iconNew = new ImageIcon(imgFINAL.getScaledInstance(300, 225, Image.SCALE_SMOOTH));
					//iconNew = new ImageIcon(imgFINAL);
					lblFinal.setIcon(iconNew);
					
					File outputfile = new File(rutaFile);
					try {
						ImageIO.write(imgFINAL, "jpg", outputfile);
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					//imgFINAL=null;
				}else{
					JOptionPane.showMessageDialog(null, "Ingrese un nombre al archivo");
				}
			}
		});
		btnGenerar.setBounds(735, 348, 89, 33);
		contentPane.add(btnGenerar);
		
		
		JLabel lblNombreDelArchivo = new JLabel("Nombre del Archivo");
		lblNombreDelArchivo.setBounds(710, 312, 134, 14);
		contentPane.add(lblNombreDelArchivo);
		
		JLabel label_3 = new JLabel("|");
		label_3.setBounds(559, 304, 4, 14);
		contentPane.add(label_3);
		
		JLabel label_4 = new JLabel("_");
		label_4.setBounds(559, 330, 15, 14);
		contentPane.add(label_4);
	}
}
