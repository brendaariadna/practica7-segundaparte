package procesamiento.op;

import java.awt.Color;
import java.awt.image.BufferedImage;

public class ImageOperator {
	//BufferedImage representa el mapa de bits y provee toda la funcionalidad para acceder y operar con los pixeles
	BufferedImage img1, img2, imgFinal;
	Color cImg1;
	Color cImg2;
	Chunk bloqueC;
	int chunkRows=0, chunkCols=0, chunkCounter=0;
	
	public ImageOperator(BufferedImage i1, BufferedImage i2)
	{
		img1 = i1;
		img2 = i2;
	}
	
	// pixel2matrix o matrix2pixel :/
	public BufferedImage generarBloques(int bX, int bY, float alpha){
		int x=0;
		int y=0;
		chunkRows=bX; //4 Filas Horizontal 
		chunkCols=bY; //6 Columnas Vertical
		
		// Para calcular la imagen mas peque�a
		int tamanioImg1 = img1.getHeight()+img1.getWidth();
		int tamanioImg2 = img2.getHeight()+img2.getWidth();
		
		//Se saca el valor del ancho y largo "al revez"
		if(tamanioImg1 <= tamanioImg2)
		{
			x = img1.getWidth()/chunkCols;
			y = img1.getHeight()/chunkRows;
		}
		else
		{
			x = img2.getWidth()/chunkCols;
			y = img2.getHeight()/chunkRows;
		}
		
		//Auxiliares para quedarse con el valor inicial
		//tanto de x como de y
		int auxX=x;
		int auxY=y;
		
		//Para obtener las posiciones de los bloques
		int posX=0;
		int posY=0;
		
		int band=0;
		
		for(int i =0;i<chunkRows;i++)
		{	
			x = auxX;
			posX = 0; // El inicio de las filas
			for(int j=0; j<chunkCols;j++)
			{
				System.out.print("\n\n Bloque: "+chunkCounter);
				System.out.print("\n x: "+x+"y: "+y);
				bloqueC = new Chunk();
				bloqueC.setCoordenada(y, x);
				System.out.print("\n In x: "+(posX)+" In y: "+(posY));
				
				switch(band)
				{
					case 0:
					{
						imgOperatorSuma(bloqueC,posX,posY);
						band = 1;
					}
						break;
					case 1:
					{
						imgOperatorResta(bloqueC,posX,posY);
						band=2;
					}
						break;
					case 2:
					{
						imgOperatorMultiplicacion(bloqueC,posX,posY);
						band=3;
					}
						break;
					case 3:
					{
						imgOperatorLineal(bloqueC,posX,posY,alpha);
						band=0;
					}
						break;
				}
				posX = x;
				x = x + auxX;
				chunkCounter++;
			}
			posY = y;
			y = auxY + y;
		}
		return imgFinal;
	}
	
	public BufferedImage generarBloques_Op(int bX, int bY, float alpha, int opc){
		int x=0;
		int y=0;
		chunkRows=bX; //4 Filas Horizontal 
		chunkCols=bY; //6 Columnas Vertical
		
		// Para calcular la imagen mas peque�a
		int tamanioImg1 = img1.getHeight()+img1.getWidth();
		int tamanioImg2 = img2.getHeight()+img2.getWidth();
		
		//Se saca el valor del ancho y largo "al revez"
		if(tamanioImg1 <= tamanioImg2)
		{
			x = img1.getWidth()/chunkCols;
			y = img1.getHeight()/chunkRows;
		}
		else
		{
			x = img2.getWidth()/chunkCols;
			y = img2.getHeight()/chunkRows;
		}
		
		//Auxiliares para quedarse con el valor inicial
		//tanto de x como de y
		int auxX=x;
		int auxY=y;
		
		//Para obtener las posiciones de los bloques
		int posX=0;
		int posY=0;
		
		for(int i =0;i<chunkRows;i++)
		{	
			x = auxX;
			posX = 0; // El inicio de las filas
			for(int j=0; j<chunkCols;j++)
			{
				System.out.print("\n\n Bloque: "+chunkCounter);
				System.out.print("\n x: "+x+"y: "+y);
				bloqueC = new Chunk();
				bloqueC.setCoordenada(y, x);
				System.out.print("\n In x: "+(posX)+" In y: "+(posY));
				
				switch(opc)
				{
					case 0:
					{
						imgOperatorSuma(bloqueC,posX,posY);
					}
						break;
					case 1:
					{
						imgOperatorResta(bloqueC,posX,posY);
					}
						break;
					case 2:
					{
						imgOperatorMultiplicacion(bloqueC,posX,posY);
					}
						break;
					case 3:
					{
						imgOperatorLineal(bloqueC,posX,posY,alpha);
					}
						break;
				}
				posX = x;
				x = x + auxX;
				chunkCounter++;
			}
			posY = y;
			y = auxY + y;
		}
		return imgFinal;
	}
	
	public Chunk obtainChunk(int x, int y){
		bloqueC = new Chunk();
		bloqueC.setCoordenada(x, y);
		
		int tamanioImg1 = img1.getHeight()+img1.getWidth();
		int tamanioImg2 = img2.getHeight()+img2.getWidth();
		
		if(tamanioImg1 < tamanioImg2)
		{
			bloqueC.setCrows(img1.getHeight()/chunkRows);
			bloqueC.setCcols(img1.getWidth()/chunkCols);
		}
		else
		{
			bloqueC.setCrows(img2.getHeight()/chunkRows);
			bloqueC.setCcols(img2.getWidth()/chunkCols);
		}
		
		return bloqueC;
	}
	
	public BufferedImage imgOperatorSuma(Chunk op, int y2, int x2){
		int tamanioImg1 = img1.getHeight()+img1.getWidth();
		int tamanioImg2 = img2.getHeight()+img2.getWidth();
		
		if(tamanioImg1 < tamanioImg2)
		{	imgFinal = img1; }else{	imgFinal = img2; }
		
		// - Se suma los mismos tipos de "canal"
		// - Si la suma llega a ser mayor de 255 es igual a 255
		
		//800 
			//600
		//imgFinal = new BufferedImage(w-x, h-y, BufferedImage.TYPE_INT_RGB);
		
		for(int y=x2; y < op.getCoordenada().getCoorX(); y++)
		{
			for(int x =y2; x<op.getCoordenada().getCoorY(); x++)
			{
				//System.out.print("\n y: "+y+ " - x: "+x);
				int rgbImg1 = img1.getRGB(x, y);	
				cImg1 = new Color(rgbImg1);
				
				int rgbImg2 = img2.getRGB(x, y);
				cImg2 = new Color(rgbImg2);
				
				int cR = cImg1.getRed() + cImg2.getRed();
				int cG = cImg1.getGreen() + cImg2.getGreen();
				int cB = cImg1.getBlue() + cImg2.getBlue();
				
				if(cR > 255)
				{	cR = 255; }
				
				if(cG > 255 )
				{ 	cG = 255; }
				
				if(cB > 255 )
				{	cB = 255; }
				
				// Para generar el �ltimo canal -> B
				Color cNew = new Color(cR, cG, cB);
				
				imgFinal.setRGB(x, y, cNew.getRGB());
				//System.out.print(" r:" +valR+ " g:" +valG+ " b:" +valB);				
			}

		}
		return imgFinal;
	}
	
	public BufferedImage imgOperatorResta(Chunk op, int y2, int x2){
		int tamanioImg1 = img1.getHeight()+img1.getWidth();
		int tamanioImg2 = img2.getHeight()+img2.getWidth();
		
		if(tamanioImg1 < tamanioImg2)
		{	imgFinal = img1; }else{	imgFinal = img2; }
		
		//800 
			//600
		for(int y=x2; y < op.getCoordenada().getCoorX(); y++)
		{
			for(int x =y2; x<op.getCoordenada().getCoorY(); x++)
			{
				int rgbImg1 = img1.getRGB(x, y);	
				Color cImg1 = new Color(rgbImg1);
				
				int rgbImg2 = img2.getRGB(x, y);
				Color cImg2 = new Color(rgbImg2);
				
				int cR = cImg1.getRed() - cImg2.getRed();
				int cG = cImg1.getGreen() - cImg2.getGreen();
				int cB = cImg1.getBlue() - cImg2.getBlue();
				
				if(cR < 0)
				{	cR = 0; }
				
				if(cG < 0 )
				{ 	cG = 0; }
				
				if(cB < 0 )
				{	cB = 0; }
				
				// Para generar el �ltimo canal -> B
				Color cNew = new Color(cR, cG, cB);
				
				imgFinal.setRGB(x, y, cNew.getRGB());
				//System.out.print(" r:" +valR+ " g:" +valG+ " b:" +valB);				
			}
		}
		return imgFinal;
	}

	public BufferedImage imgOperatorMultiplicacion(Chunk op, int y2, int x2){
		int tamanioImg1 = img1.getHeight()+img1.getWidth();
		int tamanioImg2 = img2.getHeight()+img2.getWidth();
		
		if(tamanioImg1 < tamanioImg2)
		{	imgFinal = img1; }else{	imgFinal = img2; }
		
		float b = ((float)1/(float)255);
		
		for(int y=x2; y < op.getCoordenada().getCoorX(); y++)
		{
			for(int x =y2; x<op.getCoordenada().getCoorY(); x++)
			{
				int rgbImg1 = img1.getRGB(x, y);	
				Color cImg1 = new Color(rgbImg1);
				
				int rgbImg2 = img2.getRGB(x, y);
				Color cImg2 = new Color(rgbImg2);
				
				float cR = b * cImg1.getRed() * cImg2.getRed();
				float cG = b * cImg1.getGreen() * cImg2.getGreen();
				float cB = b * cImg1.getBlue() * cImg2.getBlue();
				
				if(cR > 255)
				{	cR = 255; }
				
				if(cG > 255 )
				{ 	cG = 255; }
				
				if(cB > 255 )
				{	cB = 255; }
				
				// Para generar el �ltimo canal -> B
				Color cNew = new Color((int)(cR), (int)cG, (int)cB);
				
				imgFinal.setRGB(x, y, cNew.getRGB());
				//System.out.print(" r:" +valR+ " g:" +valG+ " b:" +valB);				
			}
		}
		return imgFinal;
	
	}
	
	public BufferedImage imgOperatorLineal(Chunk op, int y2, int x2, float alpha){
		int tamanioImg1 = img1.getHeight()+img1.getWidth();
		int tamanioImg2 = img2.getHeight()+img2.getWidth();
		
		if(tamanioImg1 < tamanioImg2)
		{	imgFinal = img1; }else{	imgFinal = img2; }
		
		float a=alpha;		
		float b=1.0f-a;
		
		for(int y=x2; y < op.getCoordenada().getCoorX(); y++)
		{
			for(int x =y2; x<op.getCoordenada().getCoorY(); x++)
			{
				int rgbImg1 = img1.getRGB(x, y);	
				Color cImg1 = new Color(rgbImg1);
				
				int rgbImg2 = img2.getRGB(x, y);
				Color cImg2 = new Color(rgbImg2);
				
				int cR = (int) ((a*cImg1.getRed()) + (b*cImg2.getRed()));
				int cG = (int) ((a*cImg1.getGreen()) + (b*cImg2.getGreen()));
				int cB = (int) ((a*cImg1.getBlue()) + (b*cImg2.getBlue()));
				
				if(cR > 255)
				{	cR = 255; }
				
				if(cG > 255 )
				{ 	cG = 255; }
				
				if(cB > 255 )
				{	cB = 255; }
				
				// Para generar el �ltimo canal -> B
				Color cNew = new Color(cR, cG, cB);
				
				imgFinal.setRGB(x, y, cNew.getRGB());
				//System.out.print(" r:" +valR+ " g:" +valG+ " b:" +valB);				
			}

		}
		return imgFinal;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
	}

}
