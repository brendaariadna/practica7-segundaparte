package procesamiento.op;

public class Chunk {
	static Coordenada cpos; 
	static int crows = 0;
	static int ccols = 0;
	
	public Chunk(){
		cpos = new Coordenada(1,1);
		crows = 1;
		ccols = 1;
	}
	
	public Coordenada getCoordenada(){
		return cpos;
	}
	
	public int getCrows(){
		return crows;
	}
	
	public int getCcols(){
		return ccols;
	}
	
	public void setCrows(int rows){
		crows = rows;
	}
	
	public void setCcols(int cols){
		ccols = cols;
	}
	
	public void setCoordenada(int x, int y){
		cpos = new Coordenada(x,y);
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Chunk c = new Chunk();
		c.setCcols(10);
		c.setCoordenada(3, 6);
		System.out.print(c.getCcols()+" - ");
		System.out.print(c.getCoordenada().getCoorY());
	}

}

