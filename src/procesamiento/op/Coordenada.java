package procesamiento.op;

public class Coordenada {
	int x=0;
	int y=0;
	
	public Coordenada(int x1, int y1){
		x = x1;
		y = y1;
	}
	
	public void setCoorX(int x1){
		x = x1;
	}
	
	public void setCoorY(int y1){
		y = y1;
	}

	public int getCoorX(){
		return x;
	}
	
	public int getCoorY(){
		return y;
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Coordenada c = new Coordenada(1,2);
		System.out.print(c.getCoorX());
	}

}
